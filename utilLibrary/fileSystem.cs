using System;
using System.IO;

namespace utilLibrary
{
    public class FileSystem 
    {
        public static void checkOutputFile(string outputFile) 
        {
            try
            {
                if (File.Exists(outputFile)) 
                {
                    File.Delete(outputFile);
                    return;
                }

                DirectoryInfo parentDir = Directory.GetParent(outputFile);
                checkPath(parentDir);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("checking output file faild: {0}", e.Message));
            }
        }

        public static void checkPath(DirectoryInfo directory) 
        {
            while (!Directory.Exists(directory.FullName)) 
            {
                checkPath(directory.Parent);
                directory.Create();
            }
        }


        private static string createFileName(string fileName, string extension)
        {
            var dateTime = DateTime.Now;
            string name = Path.GetFileNameWithoutExtension(fileName);            
            return String.Format("{0}_{1:00}{2:00}{3:00}_{4:00}{5:00}{6:00}{7:00}", 
                name, dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, extension);                
        }

        public static string createFullFileName(string path, string fileName)
        {
            string ext = Path.GetExtension(fileName);
            return createFullFileName(path, fileName, ext);
        }

        public static string createFullFileName(string path, string fileName, string fileExt)
        {
            var readyFileName = createFileName(fileName, fileExt);
            var fullFilePath = Path.Combine(path, readyFileName);
            FileSystem.checkOutputFile(fullFilePath);
            return fullFilePath;
        }
    }
}