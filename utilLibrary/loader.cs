using System;
using System.Xml;
using Config;
using System.IO;

namespace utilLibrary
{
    public class Loader
    {
        private Download downloadConfig;

        public Loader(Download downloadConfig)
        {
            this.downloadConfig = downloadConfig;
        } 

        public XmlNodeList load()
        {
            var xmlDoc = new XmlDocument();            
            
            try
            {
                Console.WriteLine("Dowloading feed: '{0}'", downloadConfig.url );
                xmlDoc.Load(downloadConfig.url);
                if (downloadConfig.saveFeed)
                    saveFeedFile(xmlDoc);
                    
                return xmlDoc.GetElementsByTagName("SHOPITEM");
            }
            catch(Exception e) {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        private void saveFeedFile(XmlDocument xmlDoc) 
        {         
            try
            {                   
                string fullFilePath = FileSystem.createFullFileName(downloadConfig.savePath, downloadConfig.url);                

                Console.WriteLine("Saving feed: '{0}'", fullFilePath);
                xmlDoc.Save(fullFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Save feed failed: {0}", e.Message);
            }
        }

    }
}