using System.Text;
using System.Collections.Generic;
using Config;

namespace utilLibrary.descriptor
{
    public class Feed
    {
        private FeedDescriptor mFeedDescriptor;
        private List<ShopItem> mItems;

        public Feed()
        {
            mFeedDescriptor = new FeedDescriptor();
            mItems = new List<ShopItem>(20000);
        }

        public void addItem(ShopItem item)
        {
            mItems.Add(item);
        }

        public FeedDescriptor feedDescriptor
        {
            get {return mFeedDescriptor;}
        }

        public List<ShopItem> items
        {
            get {return mItems;}
        }
        public object this[int index]
        {
            get { return mItems[index]; }
        }

        public string cvsHeader()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var descriptor in mFeedDescriptor.columnDescriptors)
            {                
                if (descriptor.isCollection)
                {
                    CollectionDescriptor collectionDescriptor  = descriptor as CollectionDescriptor;
                    foreach (var item in collectionDescriptor.descriptors)
                    {
                        sb.AppendFormat("{0}{1}", item.outputName, Configuration.get().export.delimiter);                      
                    } 
                }
                else                
                    sb.AppendFormat("{0}{1}",descriptor.outputName, Configuration.get().export.delimiter);                               
            }
            sb.Remove(sb.Length-1, 1); //remove last delimeter
            return sb.ToString();

        }
    }
}