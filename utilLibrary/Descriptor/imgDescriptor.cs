using System;
using System.Xml;

namespace utilLibrary.descriptor
{
    public class ImgDescriptor: ColumnDescriptor
    {
        public const string IMGURL_ALTERNATIVE_TAG = "IMGURL_ALTERNATIVE";
        public const string IMGURL_BASE_TAG = "IMGURL";
        private int mAlternativeCount;

        public ImgDescriptor(XmlElement element)
            :this(element, 0)
        {      
        }
        public ImgDescriptor(XmlElement element, int alternativeCount)
            :base(element)
        {      
            mAlternativeCount = alternativeCount;
            setOutputName(originalName);
        }

        private void setOutputName(string originalName)
        {
            if (originalName.Equals(IMGURL_ALTERNATIVE_TAG))               
                mOutputName = createAlternativeName(mAlternativeCount);
        }
        public static string createAlternativeName(int count)
        {
            return string.Format("{0}{1}", IMGURL_BASE_TAG, count); 
        }
        public bool isAlternativeImage()
        {
            return mAlternativeCount > 0;
        }
        public int alternativeCount
        {
            get {return mAlternativeCount;}
        }

    }
}