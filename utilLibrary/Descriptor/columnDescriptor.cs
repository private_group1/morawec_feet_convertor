using System;
using System.Xml;
using Config;

namespace utilLibrary.descriptor
{
    public class ColumnDescriptor: IColumnDescriptor
    {
        
        protected string mOriginalName;
        protected string mOutputName;

        protected bool mCanBeMuliLine;

        public ColumnDescriptor(XmlElement element)
        {
            mOriginalName = element.Name;
            mOutputName = originalName;
            mCanBeMuliLine = Configuration.get().parsing.multiLineElements.Exists( x => {return x.ToUpper().Equals(mOriginalName.ToUpper());});
        }

        public string originalName
        {
            get {return mOriginalName;}
        }

        public string outputName
        {
            get {return mOutputName;}
        }

        public bool isCollection 
        {
            get {return false;}
        }

        public bool canBeMuliLine
        {
            get {return mCanBeMuliLine;}
        }
    }
}