using System;
using System.Collections.Generic;
using System.Xml;

namespace utilLibrary.descriptor
{
    public class FeedDescriptor
    {
        //public static string[] multiLineItems = {"DESCRIPTION"};
        private List<IColumnDescriptor> mColumns;
        private CollectionDescriptor mImgCollectionDescriptor;
        private CollectionDescriptor mParamCollectionDescriptor;

        public FeedDescriptor()
        {
            mColumns = new List<IColumnDescriptor>(32);
            mImgCollectionDescriptor = null;
            mParamCollectionDescriptor = null;
        }

        public int count
        {
            get {return mColumns.Count;}
        }

        public List<IColumnDescriptor> columnDescriptors
        {
            get {return mColumns;}
        }

        public void addColumn(ColumnDescriptor descriptor)
        {   
            if (descriptor is ImgDescriptor)
            {
                mImgCollectionDescriptor = handleCollection(mImgCollectionDescriptor, ECollectionDescriptorType.image);                
                mImgCollectionDescriptor.addItem(descriptor);
            }    
            else if (descriptor is ParamDescriptor)
            {
                mParamCollectionDescriptor = handleCollection(mParamCollectionDescriptor, ECollectionDescriptorType.param);                
                mParamCollectionDescriptor.addItem(descriptor);                              
            }    
            else
                mColumns.Add(descriptor);    
        }
        private CollectionDescriptor handleCollection(CollectionDescriptor collectionDescriptor, ECollectionDescriptorType type)
        {
            if (collectionDescriptor == null)
            {
                collectionDescriptor = new CollectionDescriptor(type);
                mColumns.Add(collectionDescriptor);
            }
            return collectionDescriptor;
        }
        public bool containsImgCollectionDescriptor
        {
            get {return mImgCollectionDescriptor != null; }
        }

        public CollectionDescriptor imgCollectionDescriptor
        {
            get {return mImgCollectionDescriptor;}
        }

        public bool containsParamCollectionDescriptor
        {
            get {return mParamCollectionDescriptor != null;}
        }

        public CollectionDescriptor paramCollectionDescriptor
        {
            get {return mParamCollectionDescriptor;}
        }

        public bool containsColumnDescriptor(XmlElement element, int imgCount)
        {
            if (element.Name.Contains(ImgDescriptor.IMGURL_ALTERNATIVE_TAG))
                return containsImgCollectionDescriptor && mImgCollectionDescriptor.contains(ImgDescriptor.createAlternativeName(imgCount));
            if (element.Name.Contains(ParamDescriptor.PARAM_TAG))
            {
                if (!element.HasChildNodes || !element.FirstChild.Name.Equals(ParamDescriptor.PARAM_NAME_TAG))
                    throw new Exception("param element doesn't have name child");
                return containsParamCollectionDescriptor && mParamCollectionDescriptor.contains(element.FirstChild.InnerText);    
            }    
            return mColumns.Find(x => {return x.outputName.Equals(element.Name);}) != null;
        }

        public ColumnDescriptor tryToGetDescriptor(XmlElement element, int imgCount)
        {
            if (element.Name.Contains(ImgDescriptor.IMGURL_ALTERNATIVE_TAG))            
                return containsImgCollectionDescriptor ? mImgCollectionDescriptor.find(ImgDescriptor.createAlternativeName(imgCount)) : null;                                

            if (element.Name.Contains(ParamDescriptor.PARAM_TAG))
            {
                if (!element.HasChildNodes || !element.FirstChild.Name.Equals(ParamDescriptor.PARAM_NAME_TAG))
                    throw new Exception("param element doesn't have name child");

                return containsParamCollectionDescriptor ? mParamCollectionDescriptor.find(element.FirstChild.InnerText) : null;                
            }    
            return mColumns.Find(x => {return x.outputName.Equals(element.Name);}) as ColumnDescriptor;
        }

        public static ColumnDescriptor createDecsriptor(XmlElement element, int imgCount, bool isAltervativeImage)
        {
            if (isAltervativeImage)                                                
                return new ImgDescriptor(element, imgCount);                                   
            else if (element.Name.Equals(ParamDescriptor.PARAM_TAG))
                return new ParamDescriptor(element);
            else
                return new ColumnDescriptor(element);    
        }
    }
}