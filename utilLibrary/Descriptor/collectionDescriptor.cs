using System.Collections.Generic;

namespace utilLibrary.descriptor
{
    public class CollectionDescriptor : IColumnDescriptor
    {
        private List<ColumnDescriptor> mCollection;

        private ECollectionDescriptorType mType;

        public CollectionDescriptor(ECollectionDescriptorType type)
        {
            mType = type;
            mCollection = new List<ColumnDescriptor>(16);
        }

        public bool isCollection
        {
            get {return true; }
        }

        public string outputName
        {
            get {return string.Empty;}
        }

        public void addItem(ColumnDescriptor item)
        {
            mCollection.Add(item);
        } 

        public int count
        {
            get {return mCollection.Count;}
        }

        public List<ColumnDescriptor> descriptors
        {
            get {return mCollection;}
        }
        
        public bool contains(string name)
        {
            return mCollection.Find( x => {return x.outputName.Equals(name);}) != null;
        }

        public ColumnDescriptor find(string name)
        {
            return mCollection.Find( x => {return x.outputName.Equals(name);});
        }
    }
}