using System.Xml;

namespace utilLibrary.descriptor
{
    public class ColumnValue
    {
        private ColumnDescriptor mDescriptor;
        private string mValue;

        public ColumnValue(ColumnDescriptor descriptor, XmlElement element)
        {
            mDescriptor = descriptor;
            mValue = getValue(descriptor, element);            
        }

        public string value
        {
            get {return mValue;}
        }

        public ColumnDescriptor descriptor
        {
            get {return mDescriptor;}
        }

        private string getValue(ColumnDescriptor descriptor, XmlElement element)
        {
            if (descriptor is ParamDescriptor)
            {
                foreach (XmlNode item in element.ChildNodes)
                {
                    if (item.Name.Equals(ParamDescriptor.VALUE_TAG))                    
                        return item.InnerText;                                            
                }
                throw new System.Exception("parameter: " + element.Name + "does't have value"); 
            }
            else
                return element.InnerText;
        }
    }
}