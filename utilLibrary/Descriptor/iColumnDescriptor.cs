namespace utilLibrary.descriptor
{
    public interface IColumnDescriptor
    {
        public bool isCollection {get;}

        public string outputName {get;}
    } 
}