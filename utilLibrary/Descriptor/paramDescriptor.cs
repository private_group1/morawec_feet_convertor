using System;
using System.Xml;

namespace utilLibrary.descriptor
{
    public class ParamDescriptor: ColumnDescriptor
    {
        public const string PARAM_TAG = "PARAM";
        public const string PARAM_NAME_TAG = "PARAM_NAME";
        public const string VALUE_TAG = "VAL";
        public ParamDescriptor(XmlElement element)
            :base(element)
        {
            if (element.HasChildNodes)            
                setNames(element.ChildNodes);                     
            else
                throw new Exception("Wrong XmlElement. It must cointant child nodes");                      
        }
        
        private void setNames(XmlNodeList childElements)
        {
            foreach (XmlNode childElement in childElements)
            {
                if (childElement.Name.Equals(PARAM_NAME_TAG))
                {                    
                    mOriginalName = childElement.InnerText;
                    mOutputName = mOriginalName;
                    break;
                }
            }       
        }

    }
}