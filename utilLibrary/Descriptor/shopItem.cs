using System.Text;
using System.Collections.Generic;
using Config;

namespace utilLibrary.descriptor
{
    public class ShopItem
    {
        private FeedDescriptor mFeedDescriptor;
        private List<ColumnValue> mValues;

        public ShopItem(FeedDescriptor feedDescriptor)
        {
            mFeedDescriptor = feedDescriptor;
            mValues = new List<ColumnValue>(32);
        }

        public void addValue(ColumnValue value)
        {
            mValues.Add(value);
        }

        public string toCvs()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var descriptor in mFeedDescriptor.columnDescriptors)
            {                
                if (descriptor.isCollection)
                {
                    CollectionDescriptor collectionDescriptor  = descriptor as CollectionDescriptor;
                    foreach (var item in collectionDescriptor.descriptors)
                    {
                        addValueToStringBulder(sb, item);                        
                    } 
                }
                else                
                    addValueToStringBulder(sb, descriptor as ColumnDescriptor);                               
            }
            sb.Remove(sb.Length-1, 1); //remove last delimeter
            return sb.ToString();
        }

        private void addValueToStringBulder(StringBuilder stringBuilder, ColumnDescriptor descriptor)
        {
            ColumnValue columnValue = mValues.Find(x => {return x.descriptor == descriptor;});
            if (columnValue != null)
            {
                string formatStr = "{0}{1}";
                string value = columnValue.value;
                if (descriptor.canBeMuliLine)
                {
                    formatStr = "\"{0}\"{1}";
                    value = value.Replace("\"", "\"\"");
                }                 
                stringBuilder.AppendFormat(formatStr, value, Configuration.get().export.delimiter);
                
            }
            else
                stringBuilder.AppendFormat("{0}", Configuration.get().export.delimiter);
        } 
    }
}