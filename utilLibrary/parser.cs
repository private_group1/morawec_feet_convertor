using System;
using System.Xml;
using utilLibrary.descriptor;

namespace utilLibrary
{
    public class Parser
    {
        public Parser()
        {
     
        }

        public Feed parse(XmlNodeList nodeList)
        {
            Console.WriteLine("Parsing feed...");
            
            Feed feed = new Feed();
            foreach (XmlNode node in nodeList)
            {
                ShopItem shopItem = processOneNode(node as XmlElement, feed.feedDescriptor);
                feed.addItem(shopItem);
            }
            return feed;
        }

        private ShopItem processOneNode(XmlElement itemElement, FeedDescriptor feedDescriptor)
        {      
            ShopItem shopItem = new ShopItem(feedDescriptor);   
            int imgCount = 0;   
            foreach (XmlNode childNode in itemElement.ChildNodes)
            {
                try
                {
                    XmlElement element = childNode as XmlElement;
                    bool isAltervativeImage = element.Name.Equals(ImgDescriptor.IMGURL_ALTERNATIVE_TAG);
                    imgCount = isAltervativeImage ? imgCount + 1 : imgCount;

                    ColumnDescriptor columnDescriptor = feedDescriptor.tryToGetDescriptor(element, imgCount);
                    if (columnDescriptor == null)
                    {
                        columnDescriptor = FeedDescriptor.createDecsriptor(element, imgCount, isAltervativeImage);                                                
                        feedDescriptor.addColumn(columnDescriptor);
                    }   

                    ColumnValue columnValue = new ColumnValue(columnDescriptor, element);
                    shopItem.addValue(columnValue);
                }
                catch(Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            } 

            return shopItem;
        }
    }
}