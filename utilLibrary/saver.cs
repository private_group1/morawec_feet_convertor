using System;
using System.Text;
using System.IO;
using utilLibrary.descriptor;
using Config;

namespace utilLibrary
{
    public class Saver
    {
        private Configuration config;

        //public static string delimeter = ";";

        public Saver(Configuration config)
        {
            this.config = config;

        }

        private StreamWriter openFile(string outpuFile)
        {
            if (File.Exists(outpuFile))            
                File.Delete(outpuFile);  
            return new StreamWriter(outpuFile, false, new UTF8Encoding(true));
        }

        public void save(Feed feed)
        {            
            try
            {
                string outputFullFile = FileSystem.createFullFileName(config.export.outputPath, config.download.url, ".csv");
                Console.WriteLine("Saving result file: '{0}'", outputFullFile);

                var stream = openFile(outputFullFile);
                string line = feed.cvsHeader();
                stream.WriteLine(line);

                foreach (ShopItem item in feed.items)
                {
                    line = item.toCvs();
                    stream.WriteLine(line);
                }
                
                stream.Flush();
                stream.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}