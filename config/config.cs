using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Config
{
    public class Download
    {
        public string url { get; set; }
        public bool saveFeed { get; set; }
        public string savePath { get; set; }
    }

    public class Parsing
    {
        public List<string> multiLineElements { get; set; }
    }

    public class Export
    {
        public string delimiter { get; set; }
        public string outputPath { get; set; }
    }

    public class Configuration
    {
        static public string appSettingsFile = "appsettings.json";
        static Object lockObj = new Object();
        private static Configuration instance = null;

        public Download download { get; set; }
        public Parsing parsing { get; set; }
        public Export export { get; set; }

        static public Configuration get()
        {
            lock (lockObj)
            {
                if (instance != null)
                    return instance;

                if (!File.Exists(appSettingsFile))
                    throw new Exception(String.Format("Configuration file '{0}' not found", appSettingsFile));

                try
                {
                    using (StreamReader r = new StreamReader(appSettingsFile))
                    {
                        string json = r.ReadToEnd();
                        var config = JsonConvert.DeserializeObject<Configuration>(json);

                        //set apsolut paths
                        config.export.outputPath = Path.Combine(AppContext.BaseDirectory, config.export.outputPath);
                        config.download.savePath = Path.Combine(AppContext.BaseDirectory, config.download.savePath);
                        instance = config;
                        return config;
                    }
                }
                catch (Exception)
                {
                    throw new Exception("loading configuration file faild");
                }
            }
        }
    }
}