﻿using System;
using utilLibrary;
using utilLibrary.descriptor;
using System.IO;
using Config;
using Newtonsoft.Json;

namespace MorawecUtil
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reading configuration...");
            try
            {
                Configuration configuration = Configuration.get();         
                
                //load feed 
                Loader loader = new Loader(configuration.download);
                var xmlList = loader.load();

                //parse feed
                Parser parser = new Parser();
                Feed feed = parser.parse(xmlList);

                //save feed
                Saver saver = new Saver(configuration);
                saver.save(feed);
                Console.WriteLine("export successfully done");
            }
            catch (Exception e)
            {
                Console.WriteLine("program throws exception: {0}", e.Message);
            }
        }
        
    }
}
